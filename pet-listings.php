<!DOCTYPE html>
<head>
    <title>Pet Listing</title>
    <style type="text/css">
        body{
            width: 760px; /* how wide to make your web page */
            background-color: teal; /* what color to make the background */
            margin: 0 auto;
            padding: 0;
            font:12px/16px Verdana, sans-serif; /* default font */

        }
        h1 {
            font-size:36pt;
            text-align:center;
        }
        h2 {
            font-size:20pt;
            text-align:center;
        }
        div#main{
            background-color: #FFF;
            margin: 0;
            padding: 10px;
        }
    </style>
</head>
<body><div id="main">
    <h1>Pet Listing</h1>

    <?php

    $host="localhost"; // Host name
    $username="USERNAME"; // Mysql username
    $password="PASSWORD"; // Mysql password
    $db_name="petdb"; // Database name
    $tbl_name="pets"; // Table name

    // Connect to server and select databse.

    $mysqli = new mysqli('localhost', 'USERNAME', 'PASSWORD', 'petdb');
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }

    $stmt = $mysqli->prepare("select species from pets order by species group by species");
    if(!$stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($species);
    while ($stmt->fetch()){
        echo ("<ul>
               <li> $species</li>
               </ul>");
        echo '<br><br>';
    }

    $comment = $mysqli->prepare("select species,name,weight,description,picture from pets group by species");

    if(!$comment) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $comment->execute();
    $comment->bind_result($soldiers, $name,$weight,$description,$picture);

    while ($comment->fetch()){
        echo ("<table border='1'>

               <tr>
               <img> picture</tr>
               <th>species</th>
               <th>name</th>
               </tr>
               <tr>
               <td>$species</td>
               <td>$name</td>
               </tr>
               </table>");
        echo '<br><br>';
    }

    echo('<a href="add-pet.html"> add pet</a>');


    ?>



</div></body>
</html>