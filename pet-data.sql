  create database petdb;
  create table pets(
    id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    species ENUM('cat','dog','fish','bird','hamster') NOT NULL DEFAULT 'cat',
    name varchar(50) NOT NULL,
    filename varchar(150) NOT NULL,
    weight DECIMAL(4,2) NOT NULL,
    description TINYTEXT,
    primary key(id)
    ) engine = innodb default character set = utf8 collate = utf8_general_ci;